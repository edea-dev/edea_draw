Footprint
==========

.. automodule:: edea.types.pcb.footprint
   :members:
   :undoc-members:
   :show-inheritance:
