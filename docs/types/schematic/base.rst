Base
=====

.. automodule:: edea.types.schematic.base
   :members:
   :undoc-members:
   :show-inheritance:
