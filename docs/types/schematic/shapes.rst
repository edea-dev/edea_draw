Shapes
======

.. automodule:: edea.types.schematic.shapes
   :members:
   :undoc-members:
   :show-inheritance:
