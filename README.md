# edea draw

The KiCad to SVG rendering python module for the edea project.


## Installation
```sh
pip install edea_draw --index-url https://gitlab.com/api/v4/projects/52101863/packages/pypi/simple
```
or using poetry:
```sh
poetry source add --priority=explicit gitlab https://gitlab.com/api/v4/projects/52101863/packages/pypi/simple
poetry add --source gitlab edea_draw
```

## Running the tests

```sh
poetry run pytest
```
